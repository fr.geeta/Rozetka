package SignIn;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountPage {

    private final WebDriver driver;

    public AccountPage(WebDriver driver) {
        this.driver = driver;
    }
    @FindBy(id = "header_user_menu_parent")
    private WebElement signInLink;

    @FindBy(xpath = "//*[@id=\"popup_signin\"]/div[1]/div[1]/input")
    private WebElement login;

    @FindBy (xpath = "//*[@id=\"popup_signin\"]/div[1]/div[2]/div[1]/div[1]/input")
    private WebElement password;

    @FindBy (xpath = "//*[@id=\"popup_signin\"]/div[1]/div[2]/div[1]/div[1]/label/span")
    private WebElement rememberMe;

    @FindBy (xpath = "//*[@id=\"popup_signin\"]/div[1]/div[2]/div[1]/div[2]/div/span/button")
    private WebElement submitButton;

    @FindBy (xpath = "//*[@id=\"header_user_menu_parent\"]/a")
    private WebElement profileUser;


    public void waitForSignInLink(WebElement signInLink){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(signInLink));
    }

    public void waitForSignInLink(){
        waitForSignInLink(signInLink);
    }

    public void waitForProfileUser(WebElement profileUser){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(profileUser));
    }
    public void waitForProfileUser(){
        waitForProfileUser(profileUser);
    }

public void goToURL(String url){
    driver.get(url);

}
}
