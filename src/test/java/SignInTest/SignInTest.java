package SignInTest;

import SignIn.AccountPage;
import com.sun.activation.registries.MailcapParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.lang.reflect.MalformedParameterizedTypeException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class SignInTest {
    private static WebDriver driver;

@BeforeTest
    void init()throws MalformedURLException {
    DesiredCapabilities capabilities = DesiredCapabilities.chrome();
    driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
@Test
    public void test () {

    AccountPage accountPage = PageFactory.initElements(driver, AccountPage.class);
    accountPage.goToURL("https://rozetka.com.ua/ua/");
    accountPage.waitForSignInLink();

    WebElement signInLinkk = driver.findElement(By.id("header_user_menu_parent"));
    signInLinkk.click();
    WebElement login = driver.findElement(By.xpath("//*[@id=\"popup_signin\"]/div[1]/div[1]/input"));
    login.sendKeys("testusertemplate@gmail.com");
    WebElement password = driver.findElement(By.xpath("//*[@id=\"popup_signin\"]/div[1]/div[2]/div[1]/div[1]/input"));
    password.sendKeys("Testpassword1");
    WebElement rememberMe = driver.findElement(By.xpath("//*[@id=\"popup_signin\"]/div[1]/div[2]/div[1]/div[1]/label/span"));
    rememberMe.click();
    WebElement submitButton = driver.findElement(By.xpath("//*[@id=\"popup_signin\"]/div[1]/div[2]/div[1]/div[2]/div/span/button"));
    submitButton.click();

    accountPage.waitForProfileUser();

    WebElement profileUser = driver.findElement(By.xpath("//*[@id=\"header_user_menu_parent\"]/a"));
    profileUser.click();

    WebElement userName = driver.findElement(By.xpath("//*[@id=\"personal_information\"]/div/div[1]/div[2]/div/div/div[1]/div[2]/div"));
    String name = userName.getText();
    Assert.assertEquals("Test User", name);

}
    @AfterTest

    public static void logOut(){
        driver.quit();
    }
}


